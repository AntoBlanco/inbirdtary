const express = require('express'); 
let UserController = require('../controllers/users');

let router = express.Router();

router.route('/').get(UserController.index).post(UserController.create);

router.get('/new',UserController.new); // Rutas Unicas
router.route('/dataset').get(UserController.dataSetJSON);
router.route('/:id').get(UserController.show).put(UserController.update)
router.get('/:id/edit',UserController.edit);

module.exports = router;