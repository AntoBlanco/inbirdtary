const express = require('express');

let DepartmentCTRL = require("../controllers/departments");

let router = express.Router();

router.route('/').get(DepartmentCTRL.index).post(DepartmentCTRL.create);
router.route('/dataset').get(DepartmentCTRL.dataSetJSON);
router.get('/new',DepartmentCTRL.new); // Rutas Unicas
router.route('/:slug').get(DepartmentCTRL.show).put(DepartmentCTRL.update) //wildcard 
router.get('/:slug/edit',DepartmentCTRL.edit);

module.exports = router;