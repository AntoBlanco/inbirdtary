const express = require('express');
const SessionsController = require('../controllers/sessions');

let router = express.Router();

module.exports = function(passport){

    /* GET login page. */
	router.route('/signin').get(SessionsController.login).post( 
		passport.authenticate('signin', {
			failureRedirect: '/sessions/signin',
		}), SessionsController.access);

	/* GET Registration Page */
	router.route('/signup').get(SessionsController.new).post(
		passport.authenticate('signup', {
			failureRedirect: '/sessions/signup',
		}), SessionsController.register);

	/* Handle Logout */
	router.route('/signout').get(SessionsController.logout);

	return router;
}