const express = require('express'); 
let RoomTypeController = require('../controllers/roomTypes');

let router = express.Router();

router.route('/').get(RoomTypeController.index).post(RoomTypeController.create);
router.route('/dataset').get(RoomTypeController.dataSetJSON);
router.get('/new',RoomTypeController.new); // Rutas Unicas
router.route('/:slug').get(RoomTypeController.show).put(RoomTypeController.update); //wildcard 
router.get('/:slug/edit',RoomTypeController.edit);

module.exports = router;