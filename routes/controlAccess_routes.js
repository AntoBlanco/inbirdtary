const express = require('express');

let ControlAccessCTRL = require("../controllers/controlAccess");

let router = express.Router();

router.route('/').get(ControlAccessCTRL.accessDenied);

module.exports = router;