const express = require('express'); 
let LogController = require('../controllers/logs');

let router = express.Router();

router.route('/').get(LogController.index).delete(LogController.destroy);
router.route('/dataset').get(LogController.dataSetJSON);
router.route('/:id').get(LogController.show);

module.exports = router;