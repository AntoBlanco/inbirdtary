const express = require('express'); 
const AccountController = require('../controllers/accounts');

let router = express.Router();

router.route('/forget').post(AccountController.recover);
router.route('/reset/:token').get(AccountController.verify).post(AccountController.reset);
router.route('/activate/:token').get(AccountController.activate); 

module.exports = router;