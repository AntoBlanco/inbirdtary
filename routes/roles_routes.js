const express = require('express'); 
const RolesController = require('../controllers/roles');

let router = express.Router();

router.route('/').get(RolesController.index).post(RolesController.create);
router.route('/dataset').get(RolesController.dataSetJSON);
router.get('/new',RolesController.new); // Rutas Unicas
router.route('/:id').get(RolesController.show).put(RolesController.update); //wildcard 
router.get('/:id/edit',RolesController.edit);

module.exports = router;