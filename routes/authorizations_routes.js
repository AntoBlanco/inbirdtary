const express = require('express'); 
const AuthotizationController = require('../controllers/authorizations');

let router = express.Router();

router.route('/').get(AuthotizationController.index).post(AuthotizationController.create);
router.route('/dataset').get(AuthotizationController.dataSetJSON);
router.get('/new',AuthotizationController.new); // Rutas Unicas
router.route('/:id').get(AuthotizationController.show).put(AuthotizationController.update); //wildcard 
router.get('/:id/edit',AuthotizationController.edit);

module.exports = router;