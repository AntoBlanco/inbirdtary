const express = require('express'); 
let HearquarterController = require('../controllers/headquarters');

let router = express.Router();

router.route('/').get(HearquarterController.index).post(HearquarterController.create);

router.get('/new',HearquarterController.new); // Rutas Unicas
router.route('/dataset').get(HearquarterController.dataSetJSON);
router.route('/:slug').get(HearquarterController.show).put(HearquarterController.update); //wildcard 
router.get('/:slug/edit',HearquarterController.edit);

module.exports = router;