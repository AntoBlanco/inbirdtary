const express = require('express'); 
const AssetController = require('../controllers/assets');

let router = express.Router();

router.route('/').get(AssetController.index).post(AssetController.create);

router.get('/new',AssetController.new); // Rutas Unicas
router.route('/dataset').get(AssetController.dataSetJSON);
router.route('/:storeId').get(AssetController.show).put(AssetController.update); //wildcard 
router.get('/:storeId/edit',AssetController.edit);

module.exports = router;