const express = require('express');
let RoomCTRL = require("../controllers/rooms");

let router = express.Router();

router.route('/').get(RoomCTRL.index).post(RoomCTRL.create);

router.get('/new',RoomCTRL.new); // Rutas Unicas
router.route('/dataset').get(RoomCTRL.dataSetJSON);
router.route('/:slug').get(RoomCTRL.show).put(RoomCTRL.update) //wildcard 
router.get('/:slug/edit',RoomCTRL.edit);

module.exports = router;