const express = require('express'); 
const AuthotizedActionsController = require('../controllers/authorizedActions');

let router = express.Router();

router.route('/').get(AuthotizedActionsController.index).post(AuthotizedActionsController.create);
router.route('/dataset').get(AuthotizedActionsController.dataSetJSON);
router.get('/new',AuthotizedActionsController.new); // Rutas Unicas
router.route('/:id').get(AuthotizedActionsController.show).put(AuthotizedActionsController.update); //wildcard 
router.get('/:id/edit',AuthotizedActionsController.edit);

module.exports = router;