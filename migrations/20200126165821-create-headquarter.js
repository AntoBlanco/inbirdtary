'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Headquarters', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      address: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(60)
      },
      city: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      country: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(30)
      },
      slug: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(30)
      },
      state:{
        unique:false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Headquarters');
  }
};