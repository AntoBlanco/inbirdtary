'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Assets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      storeId: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(20)
      },
      name: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      brand: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      model: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      serial: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      purchaseDate: {
        unique: false,
        allowNull: false,
        type: Sequelize.DATE
      },
      amount: {
        unique: false,
        allowNull: false,
        type: Sequelize.INTEGER
      },
      departmentId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model:{
            tableName:'Departments'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      roomId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model:{
            tableName:'Rooms'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'

      },
      updatedByUserId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model:{
            tableName:'Users'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'

      },
      siigoCode: {
        unique: true,
        allowNull: true,
        type: Sequelize.INTEGER
      },
      description: {
        unique: false,
        allowNull: true,
        type: Sequelize.TEXT
      },
      state: {
        unique:false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Assets');
  }
};