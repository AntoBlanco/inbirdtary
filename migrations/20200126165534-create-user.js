'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(30)
      },
      password: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING
      },
      name: {
        unique: false,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      email: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      departmentId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model:{
            tableName: 'departments'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      roleId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model:{
            tableName: 'roles'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      state:{
        unique:false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      admin:{
        unique:false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      lastLogin: {
        type:Sequelize.DATE,
        unique: false,
        allowNull: true
      },
      resetToken: {
        type:Sequelize.STRING,
        unique: false,
        allowNull: true
      },
      resetExpires: {
        type:Sequelize.DATE,
        unique: false,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};