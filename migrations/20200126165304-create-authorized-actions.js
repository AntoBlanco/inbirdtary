'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('AuthorizedActions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      readResource: {
        unique: false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createResource: {
        unique: false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      updateResource: {
        unique: false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      deleteResource: {
        unique: false,
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      authorizationId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model:{
            tableName: 'authorizations'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      roleId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model:{
            tableName: 'roles'
          },
          key: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AuthorizedActions');
  }
};