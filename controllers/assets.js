const Asset = require('../models').Asset;
const Department = require('../models').Department;
const Room = require('../models').Room;
const User = require('../models').User;
const { Op } = require("sequelize");
var resource = "assets";

module.exports = {
    new: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("assets/new"); 
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Asset.create({
                name: req.body.name,
                brand: req.body.brand,
                model: req.body.model,
                serial: req.body.serial,
                purchaseDate: req.body.purchaseDate,
                amount: req.body.amount,
                departmentId: req.body.department,
                roomId: req.body.room,
                updatedByUserId: req.user.id,
                siigoCode: req.body.siigoCode,
                description: req.body.description
            }).then((result)=>{
                res.status(200).redirect('/assets/'+result.storeId);
            }).catch((error)=>{
                console.log(error);
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Asset.findAll({
                where:{
                    updatedByUserId:req.user.id
                },
                include: [
                    { model: Department, as:'department', attributes: ['name']},
                    { model: Room, as: 'room', attributes: ['name']},
                    { model: User, as: 'user', attributes: ['username']}
                ],
                attributes: ['id','name','brand','storeId','model','serial','purchaseDate','amount','siigoCode','description','state','createdAt','updatedAt']//Specify which fields will be exported
            }).then(function(assets){
                //console.log(req.session.passport.user);
                req.flash('info', 'Registration successfully');
                res.locals.message = req.flash();
                res.render("assets/index",{assets});
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Asset.findOne({
                where: {
                    storeId: req.params.storeId
                },include: [
                    { model: Department, as:'department', attributes: ['name']},
                    { model: Room, as: 'room', attributes: ['name']},
                    { model: User, as: 'user', attributes: ['username']}
                ]
            }).then(function(asset){
                if(asset){
                    res.render('assets/show',{asset});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Asset.findAll({
                include: [
                    { model: Department, as:'department', attributes: ['name']},
                    { model: Room, as: 'room', attributes: ['name']},
                    { model: User, as: 'user', attributes: ['username']}
                ],
                attributes: ['id','name','brand','model','serial','purchaseDate','amount','siigoCode','description','state','createdAt','updatedAt'],//Specify which fields will be exported
                raw : true
            })
            .then((assets)=>{
                res.json(assets);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req, res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Asset.findOne({
                where: {
                    storeId: req.params.storeId
                }
            }).then(function(asset){
                res.render('assets/edit',{asset});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Asset.findOne({
                where: {
                    storeId: req.params.storeId
                }
            }).then(asset=>{
                asset.name = req.body.name;
                asset.brand = req.body.brand;
                asset.model = req.body.model;
                asset.serial = req.body.serial;
                asset.purchaseDate = req.body.purchaseDate;
                asset.amount = req.body.amount;
                asset.departmentId = req.body.department;
                asset.roomId = req.body.room;
                asset.updatedByUserId = req.user.id,
                asset.siigoCode = req.body.siigoCode;
                asset.description = req.body.description;
                asset.state = req.body.state;
                asset.save().then((result)=>{
                    res.status(200).redirect('/assets/'+result.storeId);
                }).catch(error=>{
                    res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied');
    }
}