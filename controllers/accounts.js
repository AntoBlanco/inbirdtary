const User = require('../models').User;
const { Op } = require("sequelize");

module.exports = {
    recover: function(req,res){ //Valida el usuario para restablecer la contraseña
        User.findOne({
            where: {
                username: req.body.username
            }
        }).then(user=>{
            if(user){
                user.state = false; //Inhabilita el usuario para que no pueda iniciar sesión hasta que restablezca la contraseña
                user.generateNewResetToken();
                user.generateNewResetExpires();
                user.save().then(result=>{
                    result.sendResetEmail(req);
                });
                req.flash('message','Email has been sent, please check your inbox!');
            }else
                req.flash('message','The User not found, please verify...');
            res.redirect('/sessions/signin');
        }).catch(err=>{
            console.log("Error: "+err);
        });
    },
    verify: function(req,res){ //Valida que el token y el tiempo sean validos para aceptar una nueva contraseña
        User.findOne({
            where: {
                resetExpires: {
                    [Op.gt]: Date.now()
                },
                resetToken: req.params.token
            }
        }).then(user=>{
            if(user!=null)
                res.render('accounts/reset-password',{token:req.params.token, message: req.flash('message')});
            else{
                req.flash('message','URL not valid!');
                res.redirect('/sessions/signin');
            }
        }).catch(err=>{
            console.log('Error: '+err);
            req.flash('message',err);
            res.redirect('/sessions/signin');
        });
    },
    reset: function(req,res){ //Realiza validación de token y actualiza nueva contraseña
        User.findOne({
            where: {
                resetExpires: {
                    [Op.gt]: Date.now()
                },
                resetToken: req.params.token
            }//,
            //individualHooks: true //permite ejecutar hooks por cada resultados
        }).then(user=>{
            user.password_flat = req.body.password;
            user.resetToken = null;
            user.resetExpires = null;
            user.state = true;
            user.updatePassword();
            user.save().then(result=>{
                result.sendChangePasswordEmail();
                res.redirect('/sessions/signin');
            });
        }).catch(err=>{
            console.log('Error: '+err);
            req.flash('message',err);
            res.redirect('/sessions/signin');
        });
    },
    activate: function(req,res){ //find and activate new user with token
        User.findOne({
            where: {
                resetExpires: {
                    [Op.gt]: Date.now()
                },
                resetToken: req.params.token
            }
        }).then(user=>{
            user.resetToken = null;
            user.resetExpires = null;
            user.state = true;
            user.save().then(result=>{
                console.log(result);
                req.flash('message','Activation Successfull!');
                res.status(200).redirect('/sessions/signin'); // prueba estatus
            });
        }).catch(err=>{
            console.log('Error: '+err);
            req.flash('message', err);
            res.redirect('/sessions/signup');
        });
    }
}