const Room = require("../models").Room;
const RoomType = require("../models").RoomType;
const Headquarter = require("../models").Headquarter;
var resource = "assets";

module.exports = {
    new: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("rooms/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Room.create({
                name: req.body.name,
                description: req.body.description,
                headquarterId: req.body.headquarter,
                roomTypeId: req.body.room,
                state: req.body.state || true
            }).then(result=>{
                res.status(200).redirect('/rooms/'+result.slug);
            }).catch(err=>{
                res.status(400).send(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Room.findAll({
                include: [
                    { model:RoomType, as:'roomtype', attributes: ['name']},
                    { model: Headquarter, as: 'headquarter', attributes: ['name']}
                ]
            }).then((rooms)=>{
                res.render("rooms/index",{rooms});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Room.findAll({
                include: [
                    { model:RoomType, as:'roomtype', attributes: ['name']},
                    { model: Headquarter, as: 'headquarter', attributes: ['name']}
                ],
                raw: true,
                attributes: ['id','name','description','slug','state','createdAt','updatedAt']//Specify which fields will be exported
            })
            .then((rooms)=>{
                res.json(rooms);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Room.findOne({
                where: {
                    slug: req.params.slug
                },
                include: [
                    { model:RoomType, as:'roomtype' },
                    { model: Headquarter, as: 'headquarter' }
                ]
            }).then(room=>{
                if(room){
                    res.render('rooms/show',{room});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Room.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(room=>{
                res.render("rooms/edit",{room});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Room.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(room=>{
                room.name = req.body.name;
                room.description= req.body.description;
                room.headquarterId = req.body.headquarter;
                room.typeRoomId = req.body.typeRoom;
                room.state = req.body.state || true;
                room.save().then((result)=>{
                    res.status(200).redirect('/rooms/'+result.slug);
                }).catch(error=>{
                    res.status(400).send(error);
                });
            }).catch(()=>{
                res.redirect('/404');
            });
        }else
            res.redirect('/accessdenied');
    }
}