function resetSession(req, res, message){
    delete res.locals.permissions;
    req.logout();
	req.flash('message',message);
	res.redirect('/sessions/signin');
}

module.exports = {
    login: function(req,res){//Carga de pantalla de login
        res.render('sessions/signin',{message: req.flash('message')});
    },
    access: function(req,res){//redireaccionamiento a home despues de login exitoso
        res.redirect('/assets');
    },
    new: function(req, res){//Carga de pontalla de registro
        res.render('sessions/signup',{message: req.flash('message')});
    },
    register: function(req,res){//registro post registro de usuario
        resetSession(req,res,'Please, Check your email for activate account!');
    },
    logout: function(req,res,){//post salida sesión de usuario
        resetSession(req,res,'Exit, correctly!');
    }
};
