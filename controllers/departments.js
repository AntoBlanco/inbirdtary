const Department = require("../models").Department;
var resource = "departments";

module.exports = {
    new: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("departments/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Department.create({
                name: req.body.name,
                leaderName: req.body.leaderName,
                state: req.body.state || true
            }).then(result=>{
                res.status(200).redirect('/departments/'+result.slug);
            }).catch(err=>{
                res.status(400).send(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Department.findAll().then((departments)=>{
                res.render("departments/index",{departments});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Department.findAll({raw : true}).then((departments)=>{
                res.json(departments);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Department.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(department=>{
                if(department){
                    res.render('departments/show',{department});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Department.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(department=>{
                res.render("departments/edit",{department});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Department.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(department=>{
                department.name = req.body.name;
                department.leaderName = req.body.leaderName;
                department.state = req.body.state || true;
                department.save().then((result)=>{
                    res.status(200).redirect('/departments/'+result.slug);
                }).catch(error=>{
                    res.status(400).send(error);
                });
                }).catch(()=>{
                    res.redirect('/404');
                });
        }else
            res.redirect('/accessdenied');
    }
}