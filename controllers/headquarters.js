const Headquarter = require('../models').Headquarter;
var resource = "headquarters";

module.exports = {
    new: function(req, res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("headquarters/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Headquarter.create({
                name: req.body.name,
                address: req.body.address,
                city: req.body.city,
                country: req.body.country
            }).then(result=>{
                res.status(200).redirect('/headquarters/'+result.slug);
            }).catch(error=>{
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    show:function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Headquarter.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(function(headquarter){
                if(headquarter){
                    res.render('headquarters/show',{headquarter});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Headquarter.findAll().then(function(headquarters){
                res.render("headquarters/index",{headquarters});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Headquarter.findAll()
            .then((headquarters)=>{
                res.json(headquarters);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Headquarter.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(function(headquarter){
                res.render('headquarters/edit',{headquarter});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Headquarter.findOne({
                    where: {
                        slug: req.params.slug
                    }
                }).then(headquarter=>{
                    headquarter.name = req.body.name;
                    headquarter.address= req.body.address;
                    headquarter.city= req.body.city;
                    headquarter.country= req.body.country;
                    headquarter.state= req.body.state;
                    headquarter.save().then((result)=>{
                        res.status(200).redirect('/headquarters/'+result.slug);
                    }).catch(error=>{
                        res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied');
    }
};