const Authorization = require("../models").Authorization;
var resource = "authorizations";

module.exports = {
    new: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("authorizations/new"); 
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Authorization.create({
                name: req.body.name,
                place: req.body.place
            }).then((result)=>{
                res.status(200).redirect('/authorizations/'+result.id);
            }).catch((error)=>{
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Authorization.findAll().then((authorizations)=>{
                res.render("authorizations/index",{authorizations});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Authorization.findAll({raw : true}).then((authorizations)=>{
                res.json(authorizations);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Authorization.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(authorization){
                if(authorization){
                    res.render('authorizations/show',{authorization});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Authorization.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(authorization){
                res.render('authorizations/edit',{authorization});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Authorization.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then(authorization=>{
                    authorization.name = req.body.name;
                    authorization.place = req.body.place;
                    authorization.save().then((result)=>{
                        res.status(200).redirect('/authorizations/'+result.id);
                    }).catch(error=>{
                        res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied');
    },
    //Validar la consistencia de permisos si se elimina la autorización
    destroy: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.deleteResource){
            Authorization.findOne({
                where: {
                    id: req.params.id
                }
            }).then(Authorization=>{
                Authorization.destroy().then((result)=>{
                    res.redirect('/autorizaciones');
                }).catch(error=>{
                    res.status(400).send(error);
                });
            }).catch(()=>{
                res.redirect('/404');
            });
        }else
            res.redirect('/accessdenied');
    }
}