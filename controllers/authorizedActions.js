const AuthorizedAction = require("../models").AuthorizedAction;
const Role = require("../models").Role;
const Authorization = require("../models").Authorization;
var resource = "authorizedactions";

module.exports = {
    new: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("authorizedActions/new"); 
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            AuthorizedAction.create({
                readResource: req.body.read,
                createResource: req.body.create,
                updateResource: req.body.update,
                deleteResource: req.body.delete,
                authorizationId: req.body.authorization,
                roleId: req.body.role
            }).then((result)=>{
                res.status(200).redirect('/authorizedactions/'+result.id);
            }).catch((error)=>{
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            AuthorizedAction.findAll({
                include: [
                    { model:Role, as:'role',attributes:['name'] },
                    { model:Authorization, as:'authorization',attributes:['name'] }
                ],
                attributes: ['id','readResource','createResource','updateResource','deleteResource']//Specify which fields will be exported
            }).then((authorizedActions)=>{
                res.render("authorizedActions/index",{authorizedActions});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            AuthorizedAction.findAll({
                include: [
                    { model:Role, as:'role', attributes:['name']},
                    { model:Authorization, as:'authorization', attributes:['name']}
                ],
                raw : true,
                attributes: ['id','readResource','createResource','updateResource','deleteResource','createdAt','updatedAt']//Specify which fields will be exported
            }).then((authorizations)=>{
                res.json(authorizations);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            AuthorizedAction.findOne({
                where: {
                    id: req.params.id
                },
                include: [
                    { model:Role, as:'role' },
                    { model:Authorization, as:'authorization' }
                ]
            }).then(function(authorizedAction){
                if(authorizedAction){
                    res.render('authorizedActions/show',{authorizedAction});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            AuthorizedAction.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(authorizedAction){
                res.render('authorizedActions/edit',{authorizedAction});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            AuthorizedAction.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then(authorizedAction=>{
                    console.log(req.body)
                    authorizedAction.readResource = req.body.read;
                    authorizedAction.createResource = req.body.create;
                    authorizedAction.updateResource = req.body.update;
                    authorizedAction.deleteResource = req.body.delete;
                    authorizedAction.authorizationId = req.body.authorization;
                    authorizedAction.roleId = req.body.role;
                    authorizedAction.save().then((result)=>{
                        res.status(200).redirect('/authorizedactions/'+result.id);
                    }).catch(error=>{
                        res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied');
    }
}