const Log = require('../models').Log;
var resource = 'logs';

module.exports = {
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Log.findAll().then(function(logs){
                res.render("logging/index",{logs});
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Log.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(log){
                res.render("logging/show",{log});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Log.findAll({raw:true})
            .then(function(logs){
                res.json(logs);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    destroy: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.deleteResource){
            Log.destroy({
                where: {}
            }).then(response =>{
                if(response)
                    res.redirect('/logs');
                else
                    res.sendStatus('404');
            });
        }else
            res.redirect('/accessdenied');
    }
}