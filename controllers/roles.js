const Role = require("../models").Role;
var resource = "roles";

module.exports = {
    new: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("roles/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            Role.create({
                name: req.body.name
            }).then((result)=>{
                res.status(200).redirect('/roles/'+result.id);
            }).catch((error)=>{
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Role.findAll().then((roles)=>{
                res.render("roles/index",{roles});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Role.findAll({raw : true}).then((roles)=>{
                res.json(roles);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            Role.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(role){
                if(role){
                    res.render('roles/show',{role});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Role.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(role){
                res.render('roles/edit',{role});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            Role.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then(role=>{
                    role.name = req.body.name;
                    role.save().then((result)=>{
                        res.status(200).redirect('/roles/'+result.id);
                    }).catch(error=>{
                        res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied'); 
    }
}