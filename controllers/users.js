const User = require('../models').User;
const Role = require('../models').Role;
const Department = require('../models').Department;
var resource = "users";

module.exports = {
    new: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("users/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            User.create({
                email: req.body.email,
                username: req.body.username,
                password_flat: req.body.password,
                name: req.body.name,
                departmentId: req.body.department,
                roleId: req.body.role,
                admin: req.body.admin || false,
            }).then(result=>{
                res.status(200).redirect('/users/'+result.id);

            }).catch(err=>{
                res.status(400).send(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res) {
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            User.findAll({
                include:[
                    { model: Role, as: 'role', attributes: ['name'] },
                    { model: Department, as: 'department', attributes: ['name'] }
                ],
                attributes: ['id','username','name','email','state','createdAt','updatedAt']//Specify which fields will be exported
            }).then((users)=>{
                res.render("users/index",{users});
            });
        }else
            res.redirect('/accessdenied');
    },
    show: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            User.findOne({
                where: {
                    id: req.params.id
                },
                include:[
                    { model: Role, as: 'role', attributes: ['name'] },
                    { model: Department, as: 'department', attributes: ['name'] }
                ],
                attributes: ['id','username','name','email','state','admin','createdAt','updatedAt']//Specify which fields will be exported
            }).then(user=>{
                if(user){
                    res.render('users/show',{user});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            User.findAll({
                include: [
                    { model:Role, as:'role', attributes: ['name'] },
                    { model: Department, as: 'department', attributes: ['name'] }
                ],
                attributes: ['id','username','name','email','state','admin','createdAt','updatedAt'],//Specify which fields will be exported
                raw: true,
            }).then((user)=>{
                res.json(user);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');   
    },
    edit:  function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            User.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function(user){
                res.render('users/edit',{user});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            User.findOne({
                where: {
                    id: req.params.id
                }}).then(user=>{
                    user.email= req.body.email,
                    user.username= req.body.username,
                    user.password_flat= req.body.password,
                    user.name= req.body.name,
                    user.departmentId= req.body.department,
                    user.roleId= req.body.role,
                    user.state= req.body.state || true,
                    user.admin= req.body.admin || false,
                    user.save().then((result)=>{
                        res.status(200).redirect('/users/'+result.id);
                    }).catch(error=>{
                        res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
            res.redirect('/accessdenied');
    }
};