const RoomType = require('../models').RoomType;
var resource = "roomtypes";

module.exports = {
    new: function(req, res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource)
            res.render("roomTypes/new");
        else
            res.redirect('/accessdenied');
    },
    create: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.createResource){
            RoomType.create({
                name: req.body.name,
                state: req.body.state
            }).then(result=>{
                res.status(200).redirect('/roomtypes/'+result.slug);
            }).catch(error=>{
                res.status(400).send(error);
            });
        }else
            res.redirect('/accessdenied');
    },
    show:function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            RoomType.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(function(roomtype){
                if(roomtype){
                    res.render('roomTypes/show',{roomtype});
                }
                else{
                    res.sendStatus('404');
                }
            });
        }else
            res.redirect('/accessdenied');
    },
    index: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            RoomType.findAll().then(function(roomtypes){
                res.render("roomTypes/index",{roomtypes});
            });
        }else
            res.redirect('/accessdenied');
    },
    dataSetJSON: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.readResource){
            RoomType.findAll({raw:true}).then((roomtypes)=>{
                res.json(roomtypes);
            }).catch(err=>{
                res.json(err);
            });
        }else
            res.redirect('/accessdenied');
    },
    edit: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            RoomType.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(function(roomtype){
                res.render('roomTypes/edit',{roomtype});
            });
        }else
            res.redirect('/accessdenied');
    },
    update: function(req,res){
        var permissionResource = res.locals.permissions.find( item => item.place === resource)
        if(permissionResource.updateResource){
            RoomType.findOne({
                where: {
                    slug: req.params.slug
                }
            }).then(roomtype=>{
                roomtype.name = req.body.name;
                roomtype.state = req.body.state;
                roomtype.save().then((result)=>{
                    res.status(200).redirect('/roomtypes/'+result.slug);
                }).catch(error=>{
                    res.status(400).send(error);
                }).catch(()=>{
                    res.redirect('/404');
                });
            });
        }else
        res.redirect('/accessdenied');
    }
}