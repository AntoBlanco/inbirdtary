const LocalStrategy   = require('passport-local').Strategy;
let User = require('../models').User;
const { Op } = require("sequelize");

module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){
                // find a user in database with provided username
                User.findOne({
                    where: {
                        [Op.or]: [
                            {username: username},
                            {email:req.body.email}
                        ]
                    }
                }).then(user=>{
                    // already exists
                    if(user){ //validate if exist wrong username or email for alert to user
                        if (user.username == username) 
                            return done(null, false, req.flash('message','User Already Exists, Username Invalid'));
                        if (user.email == req.body.email)
                            return done(null, false, req.flash('message','User Already Exists, Email Invalid'));
                    }else {
                        // if there is no user with that email
                        // create the user
                        console.log('Creando nuevo usuario '+ req.body.username);

                        User.create({ 
                            email: req.body.email,
                            username: username,
                            password_flat: password,
                            name: req.body.name,
                            state: false,
                            departmentId: 1,
                            roleId: 1
                        }).then(user=>{
                            console.log('User Registration succesful');
                            return done(null, user);
                        }).catch(err=>{
                            console.log('Error in Saving user: '+err); 
                            throw err;  
                        });
                    }
                }).catch(err=>{
                    // In case of any error, return using the done method
                    console.log('Error in SignUp: '+err);
                    return done(err);
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );
}