const signin = require('./signin');
const signup = require('./signup');
const User = require('../models').User;
const Role = require('../models').Role;
const Department = require('../models').Department;

module.exports = function(passport){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    //determines which data of the user object should be stored in the session
    passport.serializeUser(function(user, done) {
        //console.log('serializing user: ', user);
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findOne({
            where: {
                id: id
            },
            attributes: ['id','username','name','email','departmentId','roleId','state'],
            include:[
                { model: Role, as: 'role', attributes: ['name']},
                { model: Department, as: 'department', attributes: ['name']}
            ]
        }).then(user=>{
            if(user)
                done(null, user.get()); //test
            else
                //done(user.errors(), null); //test
                done(user.errors(), null);
        }).catch(err=>{
            done(err, null);
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    signin(passport);
    signup(passport);
}