let LocalStrategy   = require('passport-local').Strategy;
let User = require('../models').User;

module.exports = function(passport){

	passport.use('signin', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) { 
            // check in database if a user with username exists or not
            User.findOne({
                where: {
                    username: username
                }
            }).then(user=>{
                // Username does not exist, log the error and redirect back
                if (!user){
                    return done(null, false, req.flash('message','User Not Found!'));             
                }
                // User exists but state false
                if(!user.state)
                    return done(null,false,req.flash('message','User Not Activated!'));
                // User exists but wrong password, log the error
                if (!user.isValidPasswordSync(password)){
                    return done(null, false, req.flash('message','Wrong Password!')); // redirect back to login page
                }
                // User and password both match, return user from done method
                // which will be treated like success
                return done(null, user);
            }).catch(err=>{
                // In case of any error, return using the done method
                return done(err);
            });
        })
    );    
}