const Authorization = require('../models').Authorization;
const AuthorizedAction = require('../models').AuthorizedAction;

module.exports = async function(req, res, next) {
	// After user is confirm is authenticated and get the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to

    if(typeof res.locals.permissions  === 'undefined'){
        await AuthorizedAction.findAll({
            where:{
                roleId: req.user.roleId
            },
            attributes:['readResource','createResource','updateResource','deleteResource'],
            include: [
                { model:Authorization, as:'authorization', attributes:['name','place'] }
            ]
        }).then((authorizedActions)=>{
            if(authorizedActions){
                console.log("Updating permissions...");
                let permissions = [];
                let item;
                for(authorizedAction of authorizedActions){
                    item = {
                        'place':authorizedAction.authorization.place,
                        'name':authorizedAction.authorization.name,
                        'readResource':authorizedAction.readResource,
                        'createResource':authorizedAction.createResource,
                        'updateResource':authorizedAction.updateResource,
                        'deleteResource':authorizedAction.deleteResource
                    }
                    permissions.push(item);
                }
                res.locals.permissions = permissions;
                return next();
            }
            else
                res.redirect('/sessions/signout');
        }).catch(err=>{
            console.log(err);
            res.status(400).send(err);
        });
    }
    else{
        return next();
    }
}