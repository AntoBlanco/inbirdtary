const Log = require('../models').Log;

module.exports = function(req,res,next){
    if(req.isAuthenticated()){
        action = "User " + req.user.username + " Action "+req.method+" - on URL: "+req.originalUrl;
        Log.create({
            action
        }).then(()=>{
        }).catch(err=>{
            next(err);
        });
    }
    next();
}