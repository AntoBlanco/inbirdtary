'use strict';

const { nanoid } = require('nanoid');

module.exports = (sequelize, DataTypes) => {
  const Asset = sequelize.define('Asset', {
    storeId: {
      type: DataTypes.STRING,
      unique: true
    },
    name: {
      type:DataTypes.STRING,
      unique: false,
      allowNull: false
    },
    brand: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: true
    },
    model: {
      type:DataTypes.STRING,
      unique: false,
      allowNull: true
    },
    serial: {
      type:DataTypes.STRING,
      unique:true,
      allowNull: true
    },
    purchaseDate: {
      type: DataTypes.DATE,
      unique: false,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      unique: false,
      defaultValue: 0,
    },
    departmentId: {
      type: DataTypes.INTEGER,
      unique: false,
      allowNull: false,
      references:{
        model:'departments',
        key:'id'
      }
    },
    roomId: {
      type: DataTypes.INTEGER,
      unique: false,
      allowNull: false,
      references: {
        model:'rooms',
        key:'id'
      }
    },
    updatedByUserId: {
      type: DataTypes.INTEGER,
      unique: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    siigoCode: {
      type:DataTypes.INTEGER,
      unique: false,
      allowNull: false
    },
    description: {
      type:DataTypes.TEXT,
      unique: false,
      defaultValue: "Sin Novedad"
    },
    state: {
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true
    }
  }, {});

  Asset.associate = function(models) { //Asociaciones entre tablas
    // associations can be defined here
    Asset.belongsTo(models.Department,{
      as: 'department',
      foreignKey: 'departmentId' //llave foranea definida en assets
    });
    Asset.belongsTo(models.Room,{
      as: 'room',
      foreignKey: 'roomId' //Llave foranea definida en assets
    });
    Asset.belongsTo(models.User,{
      as: 'user',
      foreignKey: 'updatedByUserId' //llave foranea definida en assets
    });
  };

  Asset.beforeCreate(function(asset,options){
    const company = require("../config/config").company
    asset.storeId = company.alias+"-"+nanoid(5);
  });
  return Asset;
};