'use strict';

module.exports = (sequelize, DataTypes) => {
  const AuthorizedAction = sequelize.define('AuthorizedAction', {
    readResource: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    createResource: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    updateResource: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    deleteResource: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    authorizationId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'authorizations',
        key: 'id'
      }
    },
    roleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'roles',
        key: 'id'
      }
    }
  }, {});

  AuthorizedAction.associate = function(models) { //Asociaciones entre tablas
    // associations can be defined here
    AuthorizedAction.belongsTo(models.Authorization,{
      as: 'authorization',
      foreignKey: 'authorizationId' //llave foranea definida en AuthorizationActions
    });

    AuthorizedAction.belongsTo(models.Role,{
      as: 'role',
      foreignKey: 'roleId' //llave foranea definida en AuthorizationActions
    });
  };

  return AuthorizedAction;
};