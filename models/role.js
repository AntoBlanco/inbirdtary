'use strict';

module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  }, {});
  Role.associate = function(models) {//Asociaciones entre tablas
    Role.hasMany(models.User,{
      as:'users',
      foreignKey: 'roleId',//Llave foranea definida en users
    });

    Role.hasMany(models.AuthorizedAction,{
      as:'authorizedactions',
      foreignKey: 'roleId',//Llave foranea definida en users
      onUpdate: 'CASCADE',
      //onDelete: 'CASCADE'
    });
  };
  return Role;
};