'use strict';

module.exports = (sequelize, DataTypes) => {
  const Authorization = sequelize.define('Authorization', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false
    },
    place: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false
    }
  }, {});

  Authorization.associate = function(models) { //Asociaciones entre tablas

    Authorization.hasMany(models.AuthorizedAction,{
      as:'authorizedactions',
      foreignKey: 'authorizationId', //llave definida en AuthorizationActions
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
  };
  return Authorization;
};