'use strict';
const SequelizeSlugify = require("sequelize-slugify");
var { nanoid } = require("nanoid");

module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define('Department', {
    name: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false
    },
    leaderName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    slug:{
      unique: true,
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true
    }
  }, {
    getterMethods: {
      randomId() {
        return nanoid(3);
      },
    }
  });

  Department.associate = function(models) { // Asociacion entre tablas
    // associations can be defined here
    Department.hasMany(models.Asset,{
      as: 'assets',
      foreignKey: 'departmentId' //llave foranea definida en assets
    });

    Department.hasMany(models.User,{
      as: 'users',
      foreignKey: 'departmentId' //llva foranea deifinida en users
    });
  };

  SequelizeSlugify.slugifyModel(Department, {
    source: ['name','randomId'],
    suffixSource: ['randomId']
  });

  return Department;
};