'use strict';
const SequelizeSlugify = require("sequelize-slugify");
var { nanoid } = require("nanoid");

module.exports = (sequelize, DataTypes) => {
  const Headquarter = sequelize.define('Headquarter', {
    name: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        len: {
            args: [3,50],
            msg: "El nombre de la sede debe tener minimo 3  y maximo 3 caracteres"
        }
      }
    },
    address: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        len: {
            args: [3,60],
            msg: "Dirección ingresada no es valida"
        }/*,
        fn: function(val) {
            if (val !== "calle1") throw new Error("Custom validation failed");
        }*/
      }
    },
    city: {
      unique: false,
      allowNull: false,
      type: DataTypes.STRING
    },
    country: {
      unique: false,
      allowNull: false,
      type: DataTypes.STRING
    },
    slug:{
      unique: true,
      //allowNull: false,
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true
    }
  }, {
    getterMethods: {
      randomId() {
        return nanoid(3);
      }
    }
  });

  Headquarter.associate = function(models) {//Asociaciones entre tablas
    // associations can be defined here
    Headquarter.hasMany(models.Room,{
      as: 'room',
      foreignKey: 'headquarterId' //llave foranea guardada en room
    });
  };

  SequelizeSlugify.slugifyModel(Headquarter, {
    source: ['name'],
    suffixSource: ['randomId']
  });

  return Headquarter;
};