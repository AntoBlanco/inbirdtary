'use strict';
const SequelizeSlugify = require("sequelize-slugify");
var { nanoid } = require("nanoid");

module.exports = (sequelize, DataTypes) => {
  const RoomType = sequelize.define('RoomType', {
    name: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        len: {
            args: [3,50],
            msg: "El nombre del tipo de espacio debe tener minimo 3 a maximo 50 caracteres"
        }
      }
    },
    slug:{
      unique: true,
      allowNull: true,
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true
    }                                                                                                    
  }, {
    getterMethods: {
      randomId() {
        return nanoid(3);
      }
    }
  });
  RoomType.associate = function(models) {//Asociaciones entre tablas
    // associations can be defined here
    RoomType.hasMany(models.Room,{
      as: 'rooms',
      foreignKey: 'roomTypeId'//llave foranea definida en room
    })
  };

  SequelizeSlugify.slugifyModel(RoomType, {
    source: ['name','randomId'],
    suffixSource: ['randomId']
  });

  return RoomType;
};