'use strict';
module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define('Log', {
    action: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, {});
  
  return Log;
};