'use strict';
require('dotenv').config();

const bcrypt = require("bcryptjs");
const { nanoid } = require('nanoid');
const nodemailer = require('nodemailer');
const mailtemplate = require('../config/mail-template');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      unique: false,
      //allowNull: false
    },
    password_flat: DataTypes.VIRTUAL, //variable virtual para manejar el hash de la contraseña
    name: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          msg: "Email address must be valid"
        }
      }
    },
    departmentId: {
      type: DataTypes.INTEGER,
      references:{
        model:'departments',
        key:'id'
      }
    },
    roleId:{
      type: DataTypes.INTEGER,
      references:{
        model:'roles',
        key:'id'
      }
    },
    state: {
      type:DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true,
    },
    admin: {
      type:DataTypes.BOOLEAN,
      unique: false,
      defaultValue: false,
    },
    lastLogin: {
      type:DataTypes.DATE,
      unique: false,
      allowNull: true
    },
    resetToken: {
      type:DataTypes.STRING,
      unique: false,
      allowNull: true
    },
    resetExpires: {
      type:DataTypes.DATE,
      unique: false,
      allowNull: true
    }
  }, {});

  User.prototype.isValidPasswordSync = function(password){
    return bcrypt.compareSync(password, this.password);
  }

  User.prototype.isValidPasswordAsync = function(password){
    return new Promise((res,rej)=>{
      bcrypt.compare(password,this.password,function(err,valid){
        if(err) return rej(err);
        res(valid);
      });
    });
  };

  User.prototype.generateNewResetToken = function(){
    // Ensure the activation code is unique.
    this.resetToken = nanoid(20);
  }

  User.prototype.generateNewResetExpires = function(){
    this.resetExpires = Date.now() + 3600000;
  }

  User.prototype.sendResetEmail = async function(req){ //mover utilidad
    var transport = nodemailer.createTransport(new require('../config/config').nodemailer);
    var resetEmail = {
      to: this.email,
      from: mailtemplate.reset.from,
      subject: mailtemplate.reset.subject,
      text: mailtemplate.reset.text(req.headers.host,this.resetToken),
    };
  
    await transport.sendMail(resetEmail);
  }

  User.prototype.sendChangePasswordEmail = async function(){
    var transport = nodemailer.createTransport(new require('../config/config').nodemailer);
    var changeEmail = {
      to: this.email,
      from: mailtemplate.change.from,
      subject: mailtemplate.change.subject,
      text: mailtemplate.change.text(this.email),
    };
  
    await transport.sendMail(changeEmail);
  }

  User.prototype.activationUser = async function(){
    var transport = nodemailer.createTransport(new require('../config/config').nodemailer);
    var activationEmail = {
      to: this.email,
      from: mailtemplate.activation.from,
      subject: mailtemplate.activation.subject,
      text: mailtemplate.activation.text(this.resetToken),
    };
  
    await transport.sendMail(activationEmail);
  };

  User.prototype.updatePassword = function(){ //Actualiza el password usando la función bcrypt sync
    this.password = bcrypt.hashSync(this.password_flat, 10);
  }

  User.associate = function(models) {//Asociaciones entre tablas
    // associations can be defined here
    User.hasMany(models.Asset,{
      as: 'assets',
      foreignKey: 'updatedByUserId' //llave foranea definida en assets
    });
    User.belongsTo(models.Department,{
      as: 'department',
      foreignKey: 'departmentId' //Llave foranea definida en users
    });
    User.belongsTo(models.Role,{
      as:'role',
      foreignKey: 'roleId',//Llave foranea definida en users
      //targetKey: 'userId'
    });
  };

  User.beforeCreate(function(user,options){
    return new Promise((res,rej)=>{
      if(!user.state){ //Inicia proceso de activación de usuario si este está inactivo.
        user.generateNewResetToken();
        user.generateNewResetExpires();
        user.activationUser();
      }
      if(user.password_flat){
        bcrypt.hash(user.password_flat,10, function(error,hash){
          user.password = hash;
          res();
        });
      }
      else{
        rej(Error("Error - Password not Exist!"));
      };
    });
  });

  return User;
};