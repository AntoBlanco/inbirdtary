'use strict';
const SequelizeSlugify = require("sequelize-slugify");
var { nanoid } = require("nanoid");

module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define('Room', {
    name: {
      type:DataTypes.STRING,
      unique: false,
      allowNull: false
    },
    description: {
      type:DataTypes.TEXT,
      unique: false
    },
    headquarterId: {
      type: DataTypes.INTEGER,
      references:{
        model:'headquarters',
        key:'id'
      }
    },
    roomTypeId: {
      type: DataTypes.INTEGER,
      references:{
        model:'roomtypes',
        key:'id'
      }
    },
    slug:{
      unique: true,
      //allowNull: false,
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: true
    }
  }, {
    getterMethods: {
      randomId() {
        return nanoid(3);
      }
    }
});
  Room.associate = function(models) { //Asociaciones entre tablas
    // associations can be defined here
    Room.hasMany(models.Asset,{
      as: 'assets',
      foreignKey: 'roomId'//llave foranea definida en assets
    });

    Room.belongsTo(models.Headquarter,{
      as: 'headquarter',
      foreignKey: 'headquarterId'//llave foranea definida en room
    });

    Room.belongsTo(models.RoomType,{
      as: 'roomtype',
      foreignKey: 'roomTypeId'//llave foranea definida en room
    });
  };

  SequelizeSlugify.slugifyModel(Room, {
    source: ['name','randomId'],
    suffixSource: ['randomId']
  });

  return Room;
};