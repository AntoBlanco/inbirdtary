require('dotenv').config();

module.exports = {
    reset: {
        from: process.env.COMPANY_EMAIL,
        subject: 'Inbirdtary Password Reset',
        text: function(host,resetToken){
            return `
            You are receiving this because you (or someone else) have requested the reset of the password for your account.
            Please click on the following link, or paste this into your browser to complete the process:
            http://${host}/accounts/reset/${resetToken}
            If you did not request this, please ignore this email and your password will remain unchanged.`
        },
      },
    change: {
        from: process.env.COMPANY_EMAIL,
        subject: 'Your password has been changed',
        text: function(email){
            return `This is a confirmation that the password for your account "${email}" has just been changed.`
        },
    },
    activation: {
        from: process.env.COMPANY_EMAIL,
        subject: 'Inbirdtary Activation Account',
        text: function(resetToken){
            return `
          You are receiving this because you have activate your account.
          Please click on the following link, or paste this into your browser to complete the process:
          http://localhost:3000/accounts/activate/${resetToken}`
        },
    }
}