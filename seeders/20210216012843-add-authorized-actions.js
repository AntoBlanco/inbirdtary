'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('AuthorizedActions', [
      {
        id: 1,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 1,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 2,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 3,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 4,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 5,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 5,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 6,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 6,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 7,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 7,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 8,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 8,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 9,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 9,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 10,
        readResource: true,
        createResource: true,
        updateResource: true,
        deleteResource: true,
        authorizationId: 10,
        roleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('AuthorizedActions', null, {});
  }
};
