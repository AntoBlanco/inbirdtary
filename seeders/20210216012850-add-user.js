'use strict';
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [{
      id:1,
      username: 'admin',
      password: passwordHash('admin'),
      name: 'Administrador',
      email: 'admin@inbirdtary.local',
      departmentId: 1,
      roleId: 2,
      state: true,
      lastLogin: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};

function passwordHash(plainPassword){
  return bcrypt.hashSync(plainPassword, 10);
}