const express = require("express");
const logger = require('morgan');
const createError = require("http-errors");
const path = require('path');
const bodyParser = require("body-parser");
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const passport = require('passport');
const flash = require('connect-flash');
require('dotenv').config();
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config.js')[env];


const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(methodOverride('_method'));
app.use(express.static(path.join(__dirname, 'public')));//Ruta Estatica para front
app.set('view engine', 'pug');

let sessionConfig = {
    secret: ['asdlekjaus75sxfqdqdwdw2gdas','sadrjd123478tadsqqqwe1dasdw1vasd1'], //secret para encriptar las sesiones
    expires : new Date(Date.now() + 60000), //1 Hour
    /*maxAge: 60000, //1 Hour 3600000
    cookie: { 
      maxAge : new Date(Date.now() + 60000), //1 Hour 
      expires : new Date(Date.now() + 60000), //1 Hour 
     }, */
    saveUninitialized: true, //Forces a session that is "uninitialized" to be saved to the store. A session is uninitialized when it is new but not modified  //Don't create a session for anonymous users
    resave: true, //Fuerza el guardado la sesion siempre y cuando sea modificado, esto sobre cualquier petición False //Save the session to store even if it hasn't changed
    //rolling: true, //Reset the cookie Max-Age on every request
    store: new MySQLStore({
      host: config.host,
      port: config.port,
      user: config.username,
      password: config.password,
      database: config.database
    })
}


//Manejo de sesion
app.use(session(sessionConfig));
//Flash Message
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

//Initialize passport local authentication
var initPassport = require('./passport/init');
initPassport(passport);


//Routes APP
const departmentsRoutes = require("./routes/departments_routes");
const rolesRoutes = require('./routes/roles_routes');
const AuthorizationsRoutes = require('./routes/authorizations_routes');
const AuthorizedActionsRoutes = require('./routes/authorizedActions_routes');
const usersRoutes = require('./routes/users_routes');
const roomTypesRoutes = require('./routes/roomtypes_routes');
const headquartersRoutes = require('./routes/headquarters_routes');
const roomsRoutes = require('./routes/rooms_routes');
const assetsRoutes = require('./routes/assets_routes');
const logsRoutes = require('./routes/logs_routes');
const sessionsRoutes = require("./routes/sessions_routes")(passport);
const accountsRoutes =  require("./routes/accounts_routes");
const controlAccessRoutes = require("./routes/controlAccess_routes");
//middlewares
const isAuthenticatedMiddleware = require("./middlewares/authentication");
const permissionsSession = require("./middlewares/permissionsSession");
const logActionMiddleware = require('./middlewares/log_action');



app.use("/sessions",sessionsRoutes);
app.use("/accounts",accountsRoutes);
app.use("/accessdenied",controlAccessRoutes);

//Middleware
app.use(isAuthenticatedMiddleware);
app.use(permissionsSession);
app.use(logActionMiddleware);

//testing
app.get("/",function(req,res){
	res.redirect('/assets');
});

//Rutas
app.use("/departments",departmentsRoutes);
app.use("/roles",rolesRoutes);
app.use("/authorizations",AuthorizationsRoutes);
app.use("/authorizedactions",AuthorizedActionsRoutes);
app.use("/users",usersRoutes);
app.use("/roomTypes",roomTypesRoutes);
app.use("/headquarters",headquartersRoutes);
app.use("/rooms",roomsRoutes);

app.use("/assets",assetsRoutes);
app.use("/logs",logsRoutes);
//app.use("/permisos",permissionsRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;