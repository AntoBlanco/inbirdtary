$(document).ready(function() {
    $('#indexTable').DataTable({
        "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
    });
    $('.dataTables_length').addClass('bs-select');
    $('.clickable-row').click(function() { // Add to row link location.
        window.location = $(this).data("href");
    });
    barCodeOnModal();
    loadOptionsToSelect("roles");
    loadOptionsToSelect("departments");
    loadOptionsToSelect("authorizations");
    loadOptionsToSelect("roomtypes");
    loadOptionsToSelect("headquarters");
    loadOptionsToSelect("rooms");
});

//Set Value for modal barcode
function barCodeOnModal(){
    var code = $('#barcode').data('barcode');
    $('#barcode').JsBarcode(code);
}

//Set new options based on json resource
function loadOptionsToSelect(resource){
    if($('#'+resource+'-select').length){
        getJSONdata(resource).then(result=>{
            for(var i=0;i<result.length;i++)
                if(result[i].state || typeof result[i].state == "undefined")
                    $('#'+resource+'-select').append(new Option(result[i].name,result[i].id));
            var select = $('#'+resource+'-select').data('before');
            $('#'+resource+'-select').find('option[value='+select+']').attr('selected','selected');
        });
    }
}

//Get Data Json for resource dataset
async function getJSONdata(resource){
    let response =  await  fetch("/"+resource+"/dataset");
    const data = await response.json();
    return data;
}

//Generate File CSV and URL for download
function exportCSVFile(resource){

    getJSONdata(resource).then( query =>{
        var csv = this.convertToCSV(query);

        var exportedFilename = resource + '.csv' || 'export.csv';

        let blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, exportedFilename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", exportedFilename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    });
}

 // JSON to CSV Converter
 function convertToCSV(json) {
    const items = json
    const replacer = (key, value) => value === null ? '' : value // specify how you want to handle null values here
    const header = Object.keys(items[0]);
    const csv = [
        header.join(','), // header row first
        ...items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','))
        ].join('\r\n');

    return csv
}

function asyncForm(){
    $("#formulario").bind("submit",function(){
        // Capturamnos el boton de envío
        var btnEnviar = $("#btnEnviar");
        var valBtn = btnEnviar.val;
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),
            beforeSend: function(){
                /*
                * Esta función se ejecuta durante el envió de la petición al
                * servidor.
                * */
                btnEnviar.val("Espere..."); // Para input de tipo button
                btnEnviar.attr("disabled","disabled");
            },
            complete:function(data){
                /*
                * Se ejecuta al termino de la petición
                * */
                btnEnviar.val(valBtn);
                btnEnviar.removeAttr("disabled");
            },
            success: function(data){
                /*
                * Se ejecuta cuando termina la petición y esta ha sido
                * correcta
                * */
                alert("Registro Actualizado!");
                $(location).attr('href',data);
            },
            error: function(error){
                /*
                * Se ejecuta si la peticón ha sido erronea
                * */
                let errors = error.responseJSON.errors;
                alert("Error campos invalidos");
                //Marcar en rojo los campos con error para que sean corregidos.
                $(".respuesta").html(errors[0].message);
            }
        });
        // Nos permite cancelar el envio del formulario
        return false;
    });
}